package com.heyu;

import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Description
 * @Author xuejun.zhu
 * @Date 2022-12-11 10:12
 */


@SpringBootApplication
@EnableApolloConfig //开启apollo客户端
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class,args);
    }
}
